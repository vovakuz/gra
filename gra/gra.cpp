﻿#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <time.h>
#include <iostream>
#include <string>
#include <windows.h>

using namespace std;
using namespace sf;

const double SCREEN_WIDTH = 1300;
const double SCREEN_HEIGH = 360;

int main() {

    int x, y, a, b, c, d, e, f, g, h;
    int speed = 0;
    string count;
    Text text;
    Font font;

    font.loadFromFile("times.ttf");
    text.setFont(font);
    text.setCharacterSize(60);
    text.setFillColor(Color::Yellow);
    text.setStyle(Text::Bold);

    srand(time(0));



    IpAddress ip = IpAddress::getLocalAddress();
    TcpSocket socket;
    string user;
    cin >> user;
    if (user == "host")
    {
        TcpListener listener;
        listener.listen(66666);
        cout << "..." << endl;
        if (listener.accept(socket) != Socket::Done)
            return -1;
    }
    if (user == "client")
    {
        cout << "..." << endl;
        if (socket.connect(ip, 66666, seconds(10)) != Socket::Done)
        {
            return -1;
        }
    }

    RenderWindow window(VideoMode(SCREEN_WIDTH, SCREEN_HEIGH), "RACING");

    window.setFramerateLimit(60);

    Texture background, obs1, obs2, block, barrel;
    background.loadFromFile("background1.png");
    obs1.loadFromFile("obs1.png");
    obs2.loadFromFile("obs2.png");
    block.loadFromFile("block.png");
    barrel.loadFromFile("barrel.png");

    Sprite Background(background), Racer1(obs1), Racer2(obs2), Block1(block), Block2(block), Barrel1(barrel), Barrel2(barrel), Barrel3(barrel), Barrel4(barrel);
    socket.setBlocking(false);

    Racer1.setPosition(70, 90);
    Racer2.setPosition(70, 270);
    Block1.setPosition(440, 80);
    Block2.setPosition(870, 250);
    Barrel1.setPosition(70, 160);
    Barrel2.setPosition(360, 270);
    Barrel3.setPosition(1200, 160);
    Barrel4.setPosition(1000, 80);

    while (window.isOpen())
    {
        Vector2i dmove;
        Event event;

        x = Racer1.getPosition().x;
        y = Racer1.getPosition().y;

        a = Barrel2.getPosition().x;
        b = Barrel2.getPosition().y;

        c = Barrel4.getPosition().x;
        d = Barrel4.getPosition().y;

        e = Block1.getPosition().x;
        f = Block1.getPosition().y;

        g = Block2.getPosition().x;
        h = Block2.getPosition().y;

        while (window.pollEvent(event))
        {
            if (event.type == Event::KeyPressed)
            {
                dmove.x += 4 - speed;
                if (event.key.code == Keyboard::Up) dmove.y += -2;
                if (event.key.code == Keyboard::Down) dmove.y += 2;
                if (event.key.code == Keyboard::Left) dmove.x += -6;

                break;
            }
            if (event.type == Event::Closed) {

                window.close();
                break;
            }

        }

        Packet packet;

        if (socket.receive(packet) == Socket::Done)
        {
            Vector2f pos;

            packet >> pos.x >> pos.y;
            Racer2.setPosition(pos);

            packet >> pos.x >> pos.y;
            Racer1.setPosition(pos);;
        }

        if (dmove.x != 0 || dmove.y != 0)
        {
            x += dmove.x;
            y += dmove.y;
            Racer1.setPosition(x, y);

            Packet packet;
            packet << Racer1.getPosition().x << Racer1.getPosition().y;
            packet << Racer2.getPosition().x << Racer2.getPosition().y;

            if (socket.send(packet) != Socket::Done)
            {
                return -1;
            }
        }

        if (Racer1.getPosition().y + 40 >= SCREEN_HEIGH) {
            Racer1.setPosition(Racer1.getPosition().x, SCREEN_HEIGH - 40);
        }
        if (Racer1.getPosition().y <= 0) {
            Racer1.setPosition(Racer1.getPosition().x, 0);
        }
        if (Racer1.getPosition().x + 70 >= SCREEN_WIDTH) {
            Racer1.setPosition(SCREEN_WIDTH + 70, Racer1.getPosition().y);
        }
        if (Racer1.getPosition().x <= 0) {
            Racer1.setPosition(0, Racer1.getPosition().y);
        }

        if (FloatRect(Racer1.getPosition().x, Racer1.getPosition().y, 70, 40).intersects(Block1.getGlobalBounds())) {
            Racer1.setPosition(e - 70, f + 10);
        }
        if (FloatRect(Racer1.getPosition().x, Racer1.getPosition().y, 70, 40).intersects(Block2.getGlobalBounds())) {
            Racer1.setPosition(g - 70, h + 10);
        }
        if (FloatRect(Racer2.getPosition().x, Racer2.getPosition().y, 70, 40).intersects(Block1.getGlobalBounds())) {
            Racer2.setPosition(e - 70, f + 10);
        }
        if (FloatRect(Racer2.getPosition().x, Racer2.getPosition().y, 70, 40).intersects(Block2.getGlobalBounds())) {
            Racer2.setPosition(g - 70, h + 10);
        }

        if (FloatRect(Racer1.getPosition().x, Racer1.getPosition().y, 70, 40).intersects(Barrel2.getGlobalBounds())) {
            speed = 1;
            Barrel2.setPosition(-100, -100);
        }
        if (FloatRect(Racer1.getPosition().x, Racer1.getPosition().y, 70, 40).intersects(Barrel4.getGlobalBounds())) {
            speed = 1;
            Barrel4.setPosition(-100, -100);
        }
        if (FloatRect(Racer2.getPosition().x, Racer2.getPosition().y, 70, 40).intersects(Barrel2.getGlobalBounds())) {
            speed = 1;
            Barrel2.setPosition(-100, -100);
        }
        if (FloatRect(Racer2.getPosition().x, Racer2.getPosition().y, 70, 40).intersects(Barrel4.getGlobalBounds())) {
            speed = 1;
            Barrel4.setPosition(-100, -100);
        }
        if (Racer1.getPosition().x >= SCREEN_WIDTH - 43) {
            count = "Player1 WINS!!!";
            text.setString(count);
            text.setPosition(400, 180);
        }
        if (Racer2.getPosition().x >= SCREEN_WIDTH - 43) {
            count = "Player2 WINS!!!";
            text.setString(count);
            text.setPosition(400, 180);
        }

        window.clear();
        window.draw(Background);
        window.draw(Racer1);
        window.draw(Racer2);
        window.draw(Block1);
        window.draw(Block2);
        window.draw(Barrel1);
        window.draw(Barrel2);
        window.draw(Barrel3);
        window.draw(Barrel4);
        window.draw(text);
        window.display();
    }
    return 0;
}